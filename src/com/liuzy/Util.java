package com.liuzy;

/**
 * 
 * @author liuzy
 * @version 2.0
 * @since 2015年5月22日
 */
public class Util {

	public static String bytesToHexStr(byte[] bytes) {
		StringBuilder sb = new StringBuilder();
		if (bytes == null || bytes.length == 0) {
			return null;
		}
		for (int i = 0; i < bytes.length; i++) {
			String hv = Integer.toHexString(bytes[i] & 0xFF);
			if (hv.length() == 1) {
				sb.append(0);
			}
			sb.append(hv);
		}
		return sb.toString();
	}
	
	public static byte[] hexStrToBytes(String hexString) {
		String hex = "0123456789ABCDEF";
		if (hexString == null || "".equals(hexString)) {
			return null;
		}
		int length = hexString.length() / 2;
		char[] hexChars = hexString.toUpperCase().toCharArray();
		byte[] bytes = new byte[length];
		for (int i = 0; i < length; i++) {
			int pos = i * 2;
			bytes[i] = (byte) (hex.indexOf(hexChars[pos]) << 4 | hex.indexOf(hexChars[pos + 1]));

		}
		return bytes;
	}
	
}
